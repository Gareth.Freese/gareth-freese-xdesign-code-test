package com.xdesign.takehome.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.xdesign.takehome.INVOKE_ONCE
import com.xdesign.takehome.mappers.CharacterStateModelMapper
import com.xdesign.takehome.models.network.CharacterNetworkModel
import com.xdesign.takehome.models.state.CharacterStateModel
import com.xdesign.takehome.models.state.ViewState
import com.xdesign.takehome.repositories.GameOfThronesRepository
import io.mockk.Called
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

private const val EDDARD_STARK = "Eddard Stark"
private const val TYRION_LANNISTER = "Tyrion Lannister"

class HomeViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(DelicateCoroutinesApi::class)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val characterStateObserver =
        mockk<Observer<ViewState<List<CharacterStateModel>, Exception>>>(relaxed = true)

    private val networkData = mockk<CharacterNetworkModel>()

    private val eddard = mockk<CharacterStateModel> {
        every { name } returns EDDARD_STARK
    }

    private val tyrion = mockk<CharacterStateModel> {
        every { name } returns TYRION_LANNISTER
    }

    private val gameOfThronesRepository = mockk<GameOfThronesRepository> {
        coEvery { getGotCharacters() } returns listOf(networkData)
    }
    private val stateModelMapper = mockk<CharacterStateModelMapper> {
        every { this@mockk.invoke(listOf(networkData)) } returns listOf(eddard, tyrion)
    }

    private val viewModel = HomeViewModel(gameOfThronesRepository, stateModelMapper)

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        viewModel.characterViewState.observeForever(characterStateObserver)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `given fetchGotCharacters called should emit loading state`() {
        viewModel.fetchGotCharacters()

        verify(exactly = INVOKE_ONCE) { characterStateObserver.onChanged(ViewState.Loading) }
    }

    @Test
    fun `given fetchGotCharacters called and network request successful should emit success state`() {
        viewModel.fetchGotCharacters()

        verify(exactly = INVOKE_ONCE) { stateModelMapper(listOf(networkData)) }

        verify(exactly = INVOKE_ONCE) {
            characterStateObserver.onChanged(
                ViewState.Success(
                    listOf(
                        eddard,
                        tyrion
                    )
                )
            )
        }
    }

    @Test
    fun `given fetchGotCharacters called and network request fails should emit error state`() {
        val exception = mockk<java.lang.Exception>()
        coEvery { gameOfThronesRepository.getGotCharacters() } throws exception
        viewModel.fetchGotCharacters()

        verify { stateModelMapper wasNot Called }

        verify(exactly = INVOKE_ONCE) {
            characterStateObserver.onChanged(
                ViewState.Error(
                    exception
                )
            )
        }
    }

    @Test
    fun `given getFilteredCharacterList called with search term should return list of characters containing the searched term`() {
        viewModel.fetchGotCharacters()

        verify(exactly = INVOKE_ONCE) {
            characterStateObserver.onChanged(
                ViewState.Success(
                    listOf(
                        eddard,
                        tyrion
                    )
                )
            )
        }

        viewModel.getFilteredCharacterList("edd")

        verify(exactly = INVOKE_ONCE) {
            characterStateObserver.onChanged(
                ViewState.Success(
                    listOf(
                        eddard
                    )
                )
            )
        }
    }
}