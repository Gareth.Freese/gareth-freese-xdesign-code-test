package com.xdesign.takehome.repositories

import com.xdesign.takehome.INVOKE_ONCE
import com.xdesign.takehome.models.network.CharacterNetworkModel
import com.xdesign.takehome.network.CharacterApi
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import retrofit2.Retrofit

class GameOfThronesRepositoryImplTest {

    private val retrofitService = mockk<Retrofit> {
        coEvery { create(CharacterApi::class.java).getCharacters(any()).body() } returns listOf(mockk())
    }

    private val repository = GameOfThronesRepositoryImpl(retrofitService)

    @Test
    fun `given getGotCharacters called should invoke retrofit service`() {
        runBlocking {
            repository.getGotCharacters()
        }

        coVerify(exactly = INVOKE_ONCE) { retrofitService.create(CharacterApi::class.java).getCharacters(any()) }
    }

    @Test
    fun `given getGotCharacters called and network request is a success should return list of characters`() {
        var characters: List<CharacterNetworkModel>?

        runBlocking {
            characters = repository.getGotCharacters()
        }

        assertEquals(1, characters?.size)
        assertTrue(characters?.get(0) is CharacterNetworkModel)
    }

    @Test(expected = Exception::class)
    fun `given getGotCharacters called and network request fails should throe exception`() {
        coEvery { retrofitService.create(CharacterApi::class.java).getCharacters(any()).body() } throws Exception()

        runBlocking {
            repository.getGotCharacters()
        }
    }
}