package com.xdesign.takehome.mappers

import com.xdesign.takehome.ALIVE
import com.xdesign.takehome.UNKNOWN
import com.xdesign.takehome.models.network.CharacterNetworkModel
import org.junit.Assert.assertEquals
import org.junit.Test

private const val SEASON_1 = "Season 1"
private const val SEASON_2 = "Season 2"
private const val SEASON_3 = "Season 3"
private const val SEASON_4 = "Season 4"
private const val SEASON_6 = "Season 6"
private const val SEASON_7 = "Season 7"
private const val SEASON_8 = "Season 8"
private const val UNKNOWN_SEASON = "invalid data"

private const val SEASON_LIST_1 = "I - III"
private const val SEASON_LIST_2 = "I, VIII"
private const val SEASON_LIST_3 = "I, II, VI - VIII"
private const val SEASON_LIST_4 = "II - IV, VIII"
private const val SEASON_LIST_5 = "II, III, VII, VIII"
private const val SEASON_LIST_6 = "I, II, IV, VI - VIII"

private const val NAME = "name"
private const val CULTURE = "culture"
private const val BORN = "born"
private const val DIED = "died"

class CharacterStateModelMapperTest {

    private val mapper = CharacterStateModelMapper()

    @Test
    fun `given mapper invoked with network data should map basic data correctly`() {
        val characters = mapper(listOf(getCharacterNetworkModel(tvSeries = listOf())))

        assertEquals(1, characters.size)

        assertEquals(NAME, characters[0].name)
        assertEquals(CULTURE, characters[0].culture)
        assertEquals(BORN, characters[0].born)
        assertEquals(DIED, characters[0].died)
    }

    @Test
    fun `given mapper invoked with network data should map empty data correctly`() {
        val characters = mapper(listOf(getCharacterNetworkModelWithEmptyValues()))

        assertEquals(1, characters.size)

        assertEquals(UNKNOWN, characters[0].culture)
        assertEquals(UNKNOWN, characters[0].born)
        assertEquals(ALIVE, characters[0].died)
    }

    @Test
    fun `given mapper invoked with network data should map season appearances correctly`() {
        val characters = mapper(
            listOf(
                getCharacterNetworkModel(tvSeries = listOf(SEASON_1, SEASON_2, SEASON_3)),
                getCharacterNetworkModel(tvSeries = listOf(SEASON_1, UNKNOWN_SEASON, SEASON_8)),
                getCharacterNetworkModel(
                    tvSeries = listOf(
                        SEASON_1,
                        SEASON_2,
                        SEASON_6,
                        SEASON_7,
                        SEASON_8
                    )
                ),
                getCharacterNetworkModel(tvSeries = listOf(SEASON_2, SEASON_3, SEASON_4, SEASON_8)),
                getCharacterNetworkModel(tvSeries = listOf(SEASON_2, SEASON_3, SEASON_7, SEASON_8)),
                getCharacterNetworkModel(
                    tvSeries = listOf(
                        SEASON_1,
                        SEASON_2,
                        SEASON_4,
                        SEASON_6,
                        SEASON_7,
                        SEASON_8
                    )
                )
            )
        )

        assertEquals(6, characters.size)

        assertEquals(SEASON_LIST_1, characters[0].seasonAppearances)
        assertEquals(SEASON_LIST_2, characters[1].seasonAppearances)
        assertEquals(SEASON_LIST_3, characters[2].seasonAppearances)
        assertEquals(SEASON_LIST_4, characters[3].seasonAppearances)
        assertEquals(SEASON_LIST_5, characters[4].seasonAppearances)
        assertEquals(SEASON_LIST_6, characters[5].seasonAppearances)
    }

    private fun getCharacterNetworkModel(tvSeries: List<String>) =
        CharacterNetworkModel(
            name = NAME,
            culture = CULTURE,
            born = BORN,
            died = DIED,
            tvSeries = tvSeries,
            aliases = listOf(),
            playedBy = listOf(),
            gender = ""
        )

    private fun getCharacterNetworkModelWithEmptyValues() =
        CharacterNetworkModel(
            name = NAME,
            culture = "",
            born = "",
            died = "",
            tvSeries = listOf(),
            aliases = listOf(),
            playedBy = listOf(),
            gender = ""
        )
}