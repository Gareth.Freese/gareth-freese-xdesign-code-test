package com.xdesign.takehome.mappers

import com.xdesign.takehome.ALIVE
import com.xdesign.takehome.UNKNOWN
import com.xdesign.takehome.models.network.CharacterNetworkModel
import com.xdesign.takehome.models.state.CharacterStateModel
import javax.inject.Inject

class CharacterStateModelMapper @Inject constructor() :
        (List<CharacterNetworkModel>) -> List<CharacterStateModel> {

    override fun invoke(characterNetworkData: List<CharacterNetworkModel>) =
        characterNetworkData.filter { it.name.isNotEmpty() }.map { character ->
            with(character) {
                CharacterStateModel(
                    name = name,
                    culture = culture.ifEmpty { UNKNOWN },
                    born = born.ifEmpty { UNKNOWN },
                    died = died.ifEmpty { ALIVE },
                    seasonAppearances = mapSeasonAppearanceList(tvSeries)
                )
            }
        }

    // Todo - I think there is probably a better way of doing this so would try and refactor if there was more time
    private fun mapSeasonAppearanceList(tvSeries: List<String>): String {
        val seasonList = arrayOfNulls<String>(size = 8)
        tvSeries.forEach { season ->
            when (season) {
                "Season 1" -> seasonList[0] = "I"
                "Season 2" -> seasonList[1] = "II"
                "Season 3" -> seasonList[2] = "III"
                "Season 4" -> seasonList[3] = "IV"
                "Season 5" -> seasonList[4] = "V"
                "Season 6" -> seasonList[5] = "VI"
                "Season 7" -> seasonList[6] = "VII"
                "Season 8" -> seasonList[7] = "VIII"
                else -> Unit
            }
        }
        return seasonList.joinToString(",").split(",null").mapNotNull { seasonSubString ->
            seasonSubString.split(",").toMutableList().let {
                if (it.first().isEmpty() || it.first() == "null") {
                    it.removeFirst()
                }
                when (it.size) {
                    0 -> null
                    1 -> it.first()
                    2 -> "${it.first()}, ${it.last()}"
                    else -> "${it.first()} - ${it.last()}"
                }
            }
        }.joinToString(", ")
    }
}