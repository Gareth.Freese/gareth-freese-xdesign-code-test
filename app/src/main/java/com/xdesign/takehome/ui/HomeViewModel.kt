package com.xdesign.takehome.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.xdesign.takehome.mappers.CharacterStateModelMapper
import com.xdesign.takehome.models.state.CharacterStateModel
import com.xdesign.takehome.models.state.ViewState
import com.xdesign.takehome.repositories.GameOfThronesRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val gameOfThronesRepository: GameOfThronesRepository,
    private val characterStateModelMapper: CharacterStateModelMapper
) : ViewModel() {

    private val _characterViewState =
        MutableLiveData<ViewState<List<CharacterStateModel>, Exception>>()
    val characterViewState: LiveData<ViewState<List<CharacterStateModel>, Exception>> =
        _characterViewState

    private val characterList: ArrayList<CharacterStateModel> = arrayListOf()

    fun fetchGotCharacters() {
        _characterViewState.value = ViewState.Loading
        viewModelScope.launch(emitErrorState()) {
            gameOfThronesRepository.getGotCharacters()?.let { networkData ->
                characterList.clear()
                characterList.addAll(characterStateModelMapper(networkData))
                _characterViewState.value =
                    ViewState.Success(characterList)
            } ?: emitErrorState()
        }
    }

    fun getFilteredCharacterList(searchTerm: String) {
        _characterViewState.value =
            ViewState.Success(characterList.filter { it.name.contains(searchTerm, true) })
    }

    private fun emitErrorState() =
        CoroutineExceptionHandler { _, throwable ->
            _characterViewState.value = ViewState.Error(throwable as Exception)
        }
}