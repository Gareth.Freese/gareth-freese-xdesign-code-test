package com.xdesign.takehome.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.xdesign.takehome.MainActivity
import com.xdesign.takehome.adapters.CharacterAdapter
import com.xdesign.takehome.databinding.FragmentHomeBinding
import com.xdesign.takehome.models.state.CharacterStateModel
import com.xdesign.takehome.models.state.ViewState
import javax.inject.Inject

// Todo - move to string resources
private const val ERROR_MESSAGE = "Oops something went wrong! Please try again"
private const val RETRY_TEXT = "Retry"

class HomeFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: HomeViewModel by lazy {
        ViewModelProvider(
            this,
            viewModelFactory
        )[HomeViewModel::class.java]
    }

    private lateinit var binding: FragmentHomeBinding

    private lateinit var characterAdapter: CharacterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as MainActivity).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        initObservers()
        enableSearchListener()
        viewModel.fetchGotCharacters()
    }

    private fun initObservers() {
        viewModel.characterViewState.observe(viewLifecycleOwner, ::handleCharacterStateChange)
    }

    private fun initAdapter() {
        characterAdapter = CharacterAdapter()
        with(binding.characterRecyclerView) {
            adapter = characterAdapter
            layoutManager = LinearLayoutManager(context)
        }

    }

    private fun handleCharacterStateChange(characterViewState: ViewState<List<CharacterStateModel>, Exception>) {
        when (characterViewState) {
            is ViewState.Loading -> toggleLoadingIndicator(true)
            is ViewState.Error -> showSnackBar(characterViewState.error)
            is ViewState.Success -> showCharacterList(characterViewState.result)
        }
    }

    private fun toggleLoadingIndicator(isLoading: Boolean) {
        binding.progressIndicator.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun showSnackBar(exception: Exception) {
        toggleLoadingIndicator(false)
        Snackbar.make(
            binding.homeConstraintLayout,
            ERROR_MESSAGE,
            Snackbar.LENGTH_INDEFINITE
        ).setAction(RETRY_TEXT) {
            viewModel.fetchGotCharacters()
        }.show()
    }

    private fun showCharacterList(characters: List<CharacterStateModel>) {
        toggleLoadingIndicator(false)
        characterAdapter.loadCharacterData(characters)
    }

    private fun enableSearchListener() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                viewModel.getFilteredCharacterList(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.getFilteredCharacterList(query)
                return false
            }
        })
    }
}