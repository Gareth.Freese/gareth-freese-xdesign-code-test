package com.xdesign.takehome.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.xdesign.takehome.databinding.FragmentCharacterBinding
import com.xdesign.takehome.models.state.CharacterStateModel

class CharacterAdapter : RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private val characterList: ArrayList<CharacterStateModel> = arrayListOf()

    fun loadCharacterData(characters: List<CharacterStateModel>) {
        characterList.clear()
        characterList.addAll(characters)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {

        return CharacterViewHolder(
            FragmentCharacterBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) =
        holder.bind(characterList[position])

    override fun getItemCount(): Int = characterList.size

    inner class CharacterViewHolder(private val binding: FragmentCharacterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(c: CharacterStateModel) {
            binding.apply {
                name.text = c.name
                born.text = c.born
                culture.text = c.culture
                died.text = c.died
                seasons.text = c.seasonAppearances
            }
        }
    }
}

