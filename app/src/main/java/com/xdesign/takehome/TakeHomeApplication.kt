package com.xdesign.takehome

import android.app.Application
import com.xdesign.takehome.dagger.AppComponent
import com.xdesign.takehome.dagger.DaggerAppComponent

class TakeHomeApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = initDagger()
    }

    private fun initDagger(): AppComponent =
        DaggerAppComponent.builder()
            .build()
}