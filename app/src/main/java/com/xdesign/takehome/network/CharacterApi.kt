package com.xdesign.takehome.network

import com.xdesign.takehome.models.network.CharacterNetworkModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface CharacterApi {
    @GET("/characters")
    suspend fun getCharacters(@Header("Authorization") token: String): Response<List<CharacterNetworkModel>>
}