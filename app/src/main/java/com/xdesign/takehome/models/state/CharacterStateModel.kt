package com.xdesign.takehome.models.state

data class CharacterStateModel(
    val name: String,
    val culture: String,
    val born: String,
    val died: String,
    val seasonAppearances: String
)