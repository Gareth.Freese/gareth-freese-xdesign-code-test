package com.xdesign.takehome.repositories

import com.xdesign.takehome.models.network.CharacterNetworkModel
import com.xdesign.takehome.network.CharacterApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

private const val TOKEN = "Bearer 754t!si@glcE2qmOFEcN"

class GameOfThronesRepositoryImpl @Inject constructor(
    retrofitService: Retrofit
) : GameOfThronesRepository {

    private val gotCharacterApi = retrofitService.create(CharacterApi::class.java)

    override suspend fun getGotCharacters(): List<CharacterNetworkModel>? =
        withContext(Dispatchers.IO) {
            try {
                gotCharacterApi.getCharacters(TOKEN).body()
            } catch (exception: Exception) {
                throw exception
            }
        }
}