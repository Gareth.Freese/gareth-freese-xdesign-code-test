package com.xdesign.takehome.repositories

import com.xdesign.takehome.models.network.CharacterNetworkModel

interface GameOfThronesRepository {

    suspend fun getGotCharacters(): List<CharacterNetworkModel>?
}