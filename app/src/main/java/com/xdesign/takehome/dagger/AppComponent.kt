package com.xdesign.takehome.dagger

import com.xdesign.takehome.MainActivity
import com.xdesign.takehome.ui.HomeFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ViewModelModule::class, RepositoryModule::class])
interface AppComponent {

    fun inject(activity: MainActivity?)

    fun inject(fragment: HomeFragment?)
}