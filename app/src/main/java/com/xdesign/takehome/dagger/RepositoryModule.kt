package com.xdesign.takehome.dagger

import com.xdesign.takehome.repositories.GameOfThronesRepository
import com.xdesign.takehome.repositories.GameOfThronesRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindGameOfThronesRepository(repository: GameOfThronesRepositoryImpl): GameOfThronesRepository
}